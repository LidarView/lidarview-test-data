# LidarView Test Data

This repository is used by LidarView and LidarView plugins to store test data.
Those data can then be fetched conditionally only if `BUILD_TESTING` is enabled.

To only download the necessary data, data must be upload with `git lfs` on the corresponding branch. (e.g Velodyne's data must be on a branch and a folder `velodyne`)
